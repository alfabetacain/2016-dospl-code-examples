/**
 */
package mdsebook.featuremodels1.impl;

import mdsebook.featuremodels1.Featuremodels1Package;
import mdsebook.featuremodels1.OrGroup1;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Or Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OrGroup1Impl extends Group1Impl implements OrGroup1 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrGroup1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Featuremodels1Package.Literals.OR_GROUP1;
	}

} //OrGroup1Impl
