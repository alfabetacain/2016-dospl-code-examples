/**
 */
package mdsebook.featuremodels1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.featuremodels1.Model1#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @see mdsebook.featuremodels1.Featuremodels1Package#getModel1()
 * @model
 * @generated
 */
public interface Model1 extends NamedElement1 {
	/**
	 * Returns the value of the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root</em>' containment reference.
	 * @see #setRoot(Feature1)
	 * @see mdsebook.featuremodels1.Featuremodels1Package#getModel1_Root()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Feature1 getRoot();

	/**
	 * Sets the value of the '{@link mdsebook.featuremodels1.Model1#getRoot <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root</em>' containment reference.
	 * @see #getRoot()
	 * @generated
	 */
	void setRoot(Feature1 value);

} // Model1
