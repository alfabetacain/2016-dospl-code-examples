/**
 */
package mdsebook.featuremodels1.util;

import mdsebook.featuremodels1.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see mdsebook.featuremodels1.Featuremodels1Package
 * @generated
 */
public class Featuremodels1AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Featuremodels1Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Featuremodels1AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Featuremodels1Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Featuremodels1Switch<Adapter> modelSwitch =
		new Featuremodels1Switch<Adapter>() {
			@Override
			public Adapter caseModel1(Model1 object) {
				return createModel1Adapter();
			}
			@Override
			public Adapter caseNamedElement1(NamedElement1 object) {
				return createNamedElement1Adapter();
			}
			@Override
			public Adapter caseFeature1(Feature1 object) {
				return createFeature1Adapter();
			}
			@Override
			public Adapter caseGroup1(Group1 object) {
				return createGroup1Adapter();
			}
			@Override
			public Adapter caseOrGroup1(OrGroup1 object) {
				return createOrGroup1Adapter();
			}
			@Override
			public Adapter caseXorGroup1(XorGroup1 object) {
				return createXorGroup1Adapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.Model1 <em>Model1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.Model1
	 * @generated
	 */
	public Adapter createModel1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.NamedElement1 <em>Named Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.NamedElement1
	 * @generated
	 */
	public Adapter createNamedElement1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.Feature1 <em>Feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.Feature1
	 * @generated
	 */
	public Adapter createFeature1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.Group1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.Group1
	 * @generated
	 */
	public Adapter createGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.OrGroup1 <em>Or Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.OrGroup1
	 * @generated
	 */
	public Adapter createOrGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mdsebook.featuremodels1.XorGroup1 <em>Xor Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mdsebook.featuremodels1.XorGroup1
	 * @generated
	 */
	public Adapter createXorGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Featuremodels1AdapterFactory
