/**
 */
package mdsebook.featuremodels1;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see mdsebook.featuremodels1.Featuremodels1Package
 * @generated
 */
public interface Featuremodels1Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Featuremodels1Factory eINSTANCE = mdsebook.featuremodels1.impl.Featuremodels1FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model1</em>'.
	 * @generated
	 */
	Model1 createModel1();

	/**
	 * Returns a new object of class '<em>Feature1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature1</em>'.
	 * @generated
	 */
	Feature1 createFeature1();

	/**
	 * Returns a new object of class '<em>Or Group1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Group1</em>'.
	 * @generated
	 */
	OrGroup1 createOrGroup1();

	/**
	 * Returns a new object of class '<em>Xor Group1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xor Group1</em>'.
	 * @generated
	 */
	XorGroup1 createXorGroup1();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Featuremodels1Package getFeaturemodels1Package();

} //Featuremodels1Factory
