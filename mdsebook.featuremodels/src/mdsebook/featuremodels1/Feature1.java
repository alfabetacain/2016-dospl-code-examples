/**
 */
package mdsebook.featuremodels1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.featuremodels1.Feature1#getSubfeatures <em>Subfeatures</em>}</li>
 *   <li>{@link mdsebook.featuremodels1.Feature1#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @see mdsebook.featuremodels1.Featuremodels1Package#getFeature1()
 * @model
 * @generated
 */
public interface Feature1 extends NamedElement1 {
	/**
	 * Returns the value of the '<em><b>Subfeatures</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.featuremodels1.Feature1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subfeatures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subfeatures</em>' containment reference list.
	 * @see mdsebook.featuremodels1.Featuremodels1Package#getFeature1_Subfeatures()
	 * @model containment="true"
	 * @generated
	 */
	EList<Feature1> getSubfeatures();

	/**
	 * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.featuremodels1.Group1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' containment reference list.
	 * @see mdsebook.featuremodels1.Featuremodels1Package#getFeature1_Groups()
	 * @model containment="true"
	 * @generated
	 */
	EList<Group1> getGroups();

} // Feature1
