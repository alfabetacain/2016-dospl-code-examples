/**
 */
package mdsebook.featuremodels1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.featuremodels1.Featuremodels1Package#getOrGroup1()
 * @model
 * @generated
 */
public interface OrGroup1 extends Group1 {
} // OrGroup1
