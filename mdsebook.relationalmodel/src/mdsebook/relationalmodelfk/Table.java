/**
 */
package mdsebook.relationalmodelfk;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.relationalmodelfk.Table#getPrimaryKeys <em>Primary Keys</em>}</li>
 *   <li>{@link mdsebook.relationalmodelfk.Table#getColumns <em>Columns</em>}</li>
 * </ul>
 *
 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getTable()
 * @model
 * @generated
 */
public interface Table extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Primary Keys</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.relationalmodelfk.IntegerColumn}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primary Keys</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary Keys</em>' reference list.
	 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getTable_PrimaryKeys()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IntegerColumn> getPrimaryKeys();

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.relationalmodelfk.IntegerColumn}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' containment reference list.
	 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getTable_Columns()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<IntegerColumn> getColumns();

} // Table
