/**
 */
package mdsebook.relationalmodelfk;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.relationalmodelfk.Root#getTables <em>Tables</em>}</li>
 * </ul>
 *
 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getRoot()
 * @model
 * @generated
 */
public interface Root extends EObject {
	/**
	 * Returns the value of the '<em><b>Tables</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.relationalmodelfk.Table}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tables</em>' containment reference list.
	 * @see mdsebook.relationalmodelfk.RelationalmodelfkPackage#getRoot_Tables()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Table> getTables();

} // Root
