/**
 */
package mdsebook.microEcore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EClassifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.microEcore.EClassifier#getEPackage <em>EPackage</em>}</li>
 * </ul>
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEClassifier()
 * @model abstract="true"
 * @generated
 */
public interface EClassifier extends ENamedElement {
	/**
	 * Returns the value of the '<em><b>EPackage</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link mdsebook.microEcore.EPackage#getEClassifiers <em>EClassifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EPackage</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EPackage</em>' container reference.
	 * @see #setEPackage(EPackage)
	 * @see mdsebook.microEcore.MicroEcorePackage#getEClassifier_EPackage()
	 * @see mdsebook.microEcore.EPackage#getEClassifiers
	 * @model opposite="eClassifiers" transient="false"
	 * @generated
	 */
	EPackage getEPackage();

	/**
	 * Sets the value of the '{@link mdsebook.microEcore.EClassifier#getEPackage <em>EPackage</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EPackage</em>' container reference.
	 * @see #getEPackage()
	 * @generated
	 */
	void setEPackage(EPackage value);

} // EClassifier
