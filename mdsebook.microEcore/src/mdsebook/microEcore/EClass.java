/**
 */
package mdsebook.microEcore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EClass</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.microEcore.EClass#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link mdsebook.microEcore.EClass#getInterface <em>Interface</em>}</li>
 *   <li>{@link mdsebook.microEcore.EClass#getESuperTypes <em>ESuper Types</em>}</li>
 *   <li>{@link mdsebook.microEcore.EClass#getEAllStructuralFeatures <em>EAll Structural Features</em>}</li>
 * </ul>
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEClass()
 * @model
 * @generated
 */
public interface EClass extends EClassifier {
	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see mdsebook.microEcore.MicroEcorePackage#getEClass_Abstract()
	 * @model
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link mdsebook.microEcore.EClass#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' attribute.
	 * @see #setInterface(Boolean)
	 * @see mdsebook.microEcore.MicroEcorePackage#getEClass_Interface()
	 * @model dataType="mdsebook.microEcore.EBoolean"
	 * @generated
	 */
	Object getInterface();

	/**
	 * Sets the value of the '{@link mdsebook.microEcore.EClass#getInterface <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' attribute.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(Object value);

	/**
	 * Returns the value of the '<em><b>ESuper Types</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.microEcore.EClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ESuper Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ESuper Types</em>' reference list.
	 * @see mdsebook.microEcore.MicroEcorePackage#getEClass_ESuperTypes()
	 * @model
	 * @generated
	 */
	EList<EClass> getESuperTypes();

	/**
	 * Returns the value of the '<em><b>EAll Structural Features</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.microEcore.EStructuralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EAll Structural Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EAll Structural Features</em>' containment reference list.
	 * @see mdsebook.microEcore.MicroEcorePackage#getEClass_EAllStructuralFeatures()
	 * @model containment="true"
	 * @generated
	 */
	EList<EStructuralFeature> getEAllStructuralFeatures();

} // EClass
