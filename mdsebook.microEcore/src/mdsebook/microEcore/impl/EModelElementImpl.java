/**
 */
package mdsebook.microEcore.impl;

import mdsebook.microEcore.EModelElement;
import mdsebook.microEcore.MicroEcorePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EModel Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class EModelElementImpl extends MinimalEObjectImpl.Container implements EModelElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MicroEcorePackage.Literals.EMODEL_ELEMENT;
	}

} //EModelElementImpl
