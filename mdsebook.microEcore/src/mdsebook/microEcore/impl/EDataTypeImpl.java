/**
 */
package mdsebook.microEcore.impl;

import mdsebook.microEcore.EDataType;
import mdsebook.microEcore.MicroEcorePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EData Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EDataTypeImpl extends EClassifierImpl implements EDataType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EDataTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MicroEcorePackage.Literals.EDATA_TYPE;
	}

} //EDataTypeImpl
