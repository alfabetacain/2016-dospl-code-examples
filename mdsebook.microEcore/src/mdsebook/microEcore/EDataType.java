/**
 */
package mdsebook.microEcore;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EData Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.microEcore.MicroEcorePackage#getEDataType()
 * @model
 * @generated
 */
public interface EDataType extends EClassifier {
} // EDataType
