// (c) mdsebook, wasowski, tberger

package mdsebook.pascal.scala

import scala.collection.JavaConversions._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.pascal._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Triangle] { _ => true }

  )

}
