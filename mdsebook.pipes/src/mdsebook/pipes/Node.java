/**
 */
package mdsebook.pipes;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.pipes.PipesPackage#getNode()
 * @model abstract="true"
 * @generated
 */
public interface Node extends NamedElement {
} // Node
