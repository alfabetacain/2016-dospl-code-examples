/**
 */
package mdsebook.pipes;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.pipes.Source#getSuccessors <em>Successors</em>}</li>
 * </ul>
 *
 * @see mdsebook.pipes.PipesPackage#getSource()
 * @model
 * @generated
 */
public interface Source extends Node {
	/**
	 * Returns the value of the '<em><b>Successors</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.pipes.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Successors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successors</em>' reference list.
	 * @see mdsebook.pipes.PipesPackage#getSource_Successors()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Node> getSuccessors();

} // Source
