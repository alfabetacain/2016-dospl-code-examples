/**
 */
package mdsebook.pipes;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sink</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.pipes.Sink#getPredecessors <em>Predecessors</em>}</li>
 * </ul>
 *
 * @see mdsebook.pipes.PipesPackage#getSink()
 * @model
 * @generated
 */
public interface Sink extends Node {
	/**
	 * Returns the value of the '<em><b>Predecessors</b></em>' reference list.
	 * The list contents are of type {@link mdsebook.pipes.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predecessors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessors</em>' reference list.
	 * @see mdsebook.pipes.PipesPackage#getSink_Predecessors()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Node> getPredecessors();

} // Sink
