/**
 */
package mdsebook.pipes;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mdsebook.pipes.PipesModel#getNodes <em>Nodes</em>}</li>
 * </ul>
 *
 * @see mdsebook.pipes.PipesPackage#getPipesModel()
 * @model
 * @generated
 */
public interface PipesModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link mdsebook.pipes.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see mdsebook.pipes.PipesPackage#getPipesModel_Nodes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Node> getNodes();

} // PipesModel
