// (c) mdsebook, wasowski, tberger

package mdsebook.pipes.scala

import scala.collection.JavaConversions._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.pipes._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[PipesModel] { m =>
      m.getNodes.filter { _.isInstanceOf[Source] }.size == 1 &&
        m.getNodes.filter { _.isInstanceOf[Sink] }.size == 1
    }

  )

}
