package fpinscala.parsing

import scala.language.higherKinds
import scala.language.implicitConversions
import scala.util.matching.Regex

import ImplementationTypes._

object ImplementationTypes {

  trait Result[+A] {

    def mapError (f: ParseError => ParseError) : Result[A] = this match {
      case Failure (e,c) => Failure (f(e),c)
      case _ => this
    }

    def uncommit: Result[A] = this match {
      case Failure (e,true) => Failure (e,false)
      case _ => this
    }

    def addCommit (isCommitted: => Boolean) : Result[A] = this match {
      case Failure (e,c) => Failure (e,c || isCommitted)
      case _ => this
    }

    def advanceSuccess (n: Int): Result[A] = this match {
      case Success (a,m)  => Success (a,n+m)
      case _ => this
    }
  }

  case class Success[+A] (get: A, charsConsumed: Int) extends Result[A]
  case class Failure (get: ParseError, isCommitted: Boolean = true) extends Result[Nothing]


  type Parser[+A] = Location => Result[A]
}

object MyParsers extends Parsers[Parser] {

  def run[A] (p: Parser[A]) (input: String): Either[ParseError,A] =
    p (Location(input,0)) match {
      case Success(a,n) => Right (a) // here it would be good to check that the entire input is consumed,
                                     // right now we only parse a prefix of the input
      case Failure(err,_) =>  Left (err)
    }

  // def count[A](p: fpinscala.parsing.ImplementationTypes.Parser[A]): fpinscala.parsing.ImplementationTypes.Parser[Int] = ???
  // def countPlus[A](p: fpinscala.parsing.ImplementationTypes.Parser[A]): fpinscala.parsing.ImplementationTypes.Parser[Int] = ???

  implicit def string(s: String): Parser[String] =
    loc =>
      if (loc.curr startsWith s) Success (s, s.size)
      else {
        val seen = loc.curr.substring (0, Math.min(loc.curr.size, s.size))
        Failure (loc.toError (s"expected '$s' but seen '$seen'"), false) // possibly here we could commit if at least one char matches
      }

  implicit def regex (r : Regex): Parser[String] = {
    loc => r.findPrefixOf (loc.curr) match {
      case Some (m) => Success (m, m.size)
      case None =>
        val wrong_line = loc.curr.split ("\n",2)(0)
        Failure (loc.toError (s"expected '${r.toString}', but seen '$wrong_line'"), false)
    }
  }


  def succeed[A] (a: A): Parser[A] = loc => Success (a,0)

  def slice[A] (p: Parser[A]): Parser[String] =
    loc => p(loc) match {
      case Success (a,size) => Success (loc.curr.substring(0,size),size)
      case f@Failure (_,_) => f
    }

  def scope[A] (msg: String) (p: Parser[A]): Parser[A] =
    loc => p (loc).mapError { _.push (loc,msg) }

  def label[A] (msg: String) (p: Parser[A]): Parser[A] =
    s => p (s).mapError { _ label msg }

  def attempt[A] (p: Parser[A]): Parser[A] = s => p(s).uncommit

  def or[A] (s1: Parser[A], s2: => Parser[A]): Parser[A] =
    loc => s1 (loc) match {
      case Failure (e,false) => s2 (loc)
      case r => r
    }

  def flatMap[A,B] (p: Parser[A]) (f: A => Parser[B]): Parser[B] =
    loc => p(loc) match {
      case Success (a,n) =>
        f(a) (loc advanceBy n) addCommit (n!=0) advanceSuccess (n)
      case e@Failure(_,_) => e
    }
}


