package fpinscala.parsing

import scala.language.higherKinds
import scala.language.implicitConversions
import scala.util.matching.Regex

class JSON_Parser[Parser[+_]] (P: Parsers[Parser]) {

  import P._

  val QUOTED: Parser[String] =
    """"([^"]*)"""".r
      .map { _ dropRight 1 substring 1}

  val DOUBLE: Parser[Double] =
    """(\+|-)?[0-9]+(\.[0-9]+(e[0-9]+)?)?""".r
      .map { _.toDouble }

  val jnull: Parser[JSON] =
    ws.? |* string ("null") |* succeed (JNull)

  val jbool: Parser[JBool] =
    (ws.? |* "true" |* succeed (JBool(true ))) |
    (ws.? |* "false"|* succeed (JBool(false)))

  val jstring: Parser[JString] =
    QUOTED
      .map { JString (_) }

  val jnumber: Parser[JNumber] =
    DOUBLE
      .map { JNumber (_) }

  lazy val jarray: Parser[JArray] =
    (ws.? |* "[" |* (ws.? |* json *| ",").* *| ws.? *| "]" *| ws.? )
      .map { _.toVector}.map { JArray (_) }

  lazy val field: Parser[(String, JSON)] =
    ws.? |* QUOTED *| ws.? *| ":" *| ws.? ** json *| ","

  lazy val jobject: Parser[JObject] =
    (ws.? |* "{" |* field.* *| ws.? *| "}" *| ws.?)
      .map { _.toMap }.map { JObject(_) }

  lazy val json : Parser[JSON] =
    (jstring | jobject | jarray | jnull | jnumber | jbool ) *| ws.?

}

trait JSON
case object JNull extends JSON
case class JNumber (get: Double) extends JSON
case class JString (get: String) extends JSON
case class JBool (get: Boolean) extends JSON
case class JArray (get: IndexedSeq[JSON]) extends JSON
case class JObject (get: Map[String, JSON]) extends JSON

