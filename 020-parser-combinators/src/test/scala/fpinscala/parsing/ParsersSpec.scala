package fpinscala.parsing

import scala.language.higherKinds
import scala.language.implicitConversions
import scala.util.matching.Regex

import org.scalatest.FreeSpec
import org.scalatest.Matchers

import org.scalacheck._
import org.scalatest.prop.Checkers
import org.scalacheck.Prop._
import Arbitrary.arbitrary


class ParsersSpec extends FreeSpec with Matchers with Checkers {

  import MyParsers._
  import MyParsers.ParserOps
  import ImplementationTypes.Parser

   val INT_ :Parser[Either[Int,String]] = DEC map { Left(_) }
   val ID   :Parser[String]             = MyParsers.regex ("""[a-zA-Z]+""".r)
   val ID_  :Parser[Either[Int,String]] = ID map { Right(_) }
   val INTID = INT_ or ID_

  "ws" - {
    "000 prefix parse" in { ws run " -134 alamakota " shouldBe Right(()) }
    "001 prefix parse" in { ws run "\nalamakota"      shouldBe Right(()) }
    "002 prefix parse" in { ws run "\t \n-134"        shouldBe Right(()) }
    "003 syntax error" in { ws run "42"           shouldBe a [Left[_,_]] }
  }

  "char" - {
    "010 identity" in check {
      forAll { (c: Char) => char(c).run (c.toString) == Right (c)} }
  }

  "string" - {
    "020 identity" in check {
      forAll { (s: String) => string (s).run (s) == Right(s) } }
    "021 input shorter than string" in {
      "abracadabra" run "abra" shouldBe a [Left[_,_]] }
  }

  "or" - {
    "030 choice-left" in {
      ("abra" | "cadabra") run ("abra") shouldBe Right ("abra")}
    "031 choice-right" in {
      ("abra" | "cadabra") run ("cadabra") shouldBe Right ("cadabra")}
    "032 choice-fail" in {
      ("abra" | "cadabra") run ("dabra") shouldBe a [Left[_,_]] }

    // if we want associativity then it is likely fine with left to right matching
  }

  "numA" - {
    "040" in { numA run ("b") shouldBe Right (0) }
    "041" in { numA run ("aaa") shouldBe Right (3) }
  }

  "listOfN" - {
    "050" in {
      listOfN(3, "ab" | "cad") run ("ababcad") shouldBe Right(List("ab","ab", "cad")) }
    "051" in {
      listOfN(3, "ab" | "cad") run ("cadabab") shouldBe Right(List("cad","ab","ab")) }
    "052" in {
      listOfN(3, "ab" | "cad") run ("ababab")  shouldBe Right(List("ab","ab","ab"))  }
  }

  "slice" - {
    "060" - { slice(("a"|"b").many) run ("aaba") shouldBe Right("aaba") } }

  "succeed" - { // a kind of case that Ahmad would like
    "070 string" in check {
      forAll { (s: String, q: String) => succeed (s).run (q) == Right(s) } }
    "071 int" in check {
      forAll { (s: String, n: Int) => succeed (n).run (s) == Right(n) } }
  }

  "product" - {
    "080 correct" in { DEC ** ID run ("42ala") shouldBe Right (42 -> "ala") }
    "081 incorrect" in { DEC ** ID run ("ala42") shouldBe a [Left[_,_]] }
    "*|" - {
      "082" in {
        attempt (INTID *| ws) | INTID run ("42") shouldBe Right(Left(42)) }
      "083" in {
        (INTID *| ws) or INTID run ("42 ") shouldBe Right(Left(42)) }
      "084" in {
        (INTID *| ws) or INTID run ("42ala") shouldBe a [Left[_,_]] }
      "085" in {
        (INTID *| ws) or INTID run ("42 ala ") shouldBe Right(Left(42)) }
      "086" in {
        attempt (INTID *| ws) or INTID run ("ala") shouldBe Right(Right("ala")) }
    }
  }

  "flatMap" - {
    "100 sequencing" in {
      string ("-134") flatMap { n => ID } run ("-134ala") shouldBe Right("ala") }
    "101 sequencing" in {
      DEC flatMap { n => ID } run ("+42ala") shouldBe Right("ala") }
  }

  "many" - {
    "110 three chars" in { "a".many run ("aaa") shouldBe Right(List("a", "a", "a")) }
    "111 three chars" in { "a".many run ("aaab") shouldBe Right(List("a", "a", "a")) }
    "112 one char" in { "a".many run ("a") shouldBe Right(List("a")) }
    "113 one char" in { "a".many run ("ab") shouldBe Right(List("a")) }
    "114 empty" in { "a".many run ("b") shouldBe Right(Nil) }
    "115 empty" in { "a".many run ("") shouldBe Right(Nil) }
  }

  "opt" - {

    "120 some" in { opt("aa") run ("aa") shouldBe Right(Some("aa")) }
    "121 none" in { opt("aa") run ("bb") shouldBe Right(None) }
    "122 empty" in { opt("aa") run ("") shouldBe Right(None) }
    "123 opt composed" in {
        "aa".? ** "bb" run ("aabb") shouldBe Right (Some ("aa") -> "bb") }

  }

}

class JsonSpec extends FreeSpec with Matchers {

  import ImplementationTypes.Parser
  import MyParsers._

  // for simplicty require that all lines end with commas in objects

  val test0 = """|{
                 |  "Company name" : "Microsoft Corporation",
                 |  "Ticker" : "MSFT",
                 |
                 |  "Active" : true,
                 |  "Price"   : 30.66,
                 |  "Shares outstanding" : 8.38e9,
                 |  "Related companies" :
                 |    [ "HPQ", "IBM", "YHOO", "DELL", "GOOG", ],
                 |}""".stripMargin

  val test1 = """|{
                 |  "Company name" : "Microsoft Corporation",
                 |}""".stripMargin

  val test2 = ""

  val P = new JSON_Parser (MyParsers)
  import P._

  "000" in { QUOTED run ("\"alamakota\"") shouldBe Right("alamakota") }
  "001" in { QUOTED run ("\"\"") shouldBe Right("") }

  "002" in {
    field.run ("""  "Company name" : "Microsoft Corporation", """) shouldBe
      Right("Company name" -> JString("Microsoft Corporation")) }

  "003" in {
    (field.many) run ("""  "Company name" : "Microsoft Corporation", """) shouldBe
      Right(List("Company name" -> JString("Microsoft Corporation"))) }

  "004 test1" in {
    json.run (test1) shouldBe
      Right(JObject(Map("Company name" -> JString ("Microsoft Corporation")))) }

   // the grammar right now does not allow empty input
  "005 test2" in { json.run (test2) shouldBe a [Left[_,_]] }

  "006 test1" in {
    val ast = json.run (test0)
    ast shouldBe
      Right(JObject(Map("Shares outstanding" -> JNumber(8.38E9),
                        "Price" -> JNumber(30.66),
                        "Company name" -> JString("Microsoft Corporation"),
                        "Related companies" -> JArray(
                          Vector(JString("HPQ"), JString("IBM"), JString("YHOO"),
                                 JString("DELL"), JString("GOOG"))),
                        "Ticker" -> JString("MSFT"),
                        "Active" -> JBool(true)))) }


}
