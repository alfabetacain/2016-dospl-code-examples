// (c) mdsebook, wasowski, tberger

package mdsebook.printers.scala

import scala.collection.JavaConversions._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.printers._

object Constraints {

 /* We put constraints for all the six meta-models on one list for simplicity
  * (the context type will ensure that the constraints are only applied to
  * instances of the right models anyways -- and we don't have a performance
  * issue in this exercise)
  */

	val invariants = List[(String,Constraint)] (

		"Printer is mandatory" -> inv[T1.PrinterPool] { _.getPrinter != null },
		// satisfied by t1-01, t1-03, violated by t1-00 and t1-02
		// Test cases in mdsebook.printers/test-files.

		"Fax requires a printer" -> inv[T1.PrinterPool] { self =>
			self.getFax != null implies self.getPrinter != null },
		// satisfied by t1-00, t1-01, t1-03, violated by t1-02

		"If you have a fax then you have a printer" ->
			inv[T1.Fax] { _.getPool.getPrinter != null },
		// satisfied by t1-00, t1-01, t1-03, violated by t1-02

		"A printer pool with a fax has printer, and with a copier has scanner+printer"
			-> inv[T2.PrinterPool] { self =>
			(self.getFax!=null implies self.getPrinter!=null) &&
			(self.getCopier!=null implies
				self.getScanner!=null && self.getPrinter!=null)
		},
		// satisfied by: t2-00, t2-02, violated by t2-01

		"PrinterPool’s minimum speed must be 300 lower than its regular speed" ->
			inv[T3.PrinterPool] { self => self.getMinSpeed == self.getSpeed - 300 },
		// satisfied by: none, violated by t3-00, t3-01

		"Every color printer has a colorPrinterHead" ->
			inv[T4.Printer] { self => self.isColor implies self.getHead!=null },
		// satisfied by t4-01, violated by t4-00

		"A color-capable printer pool contains at least one color-capable printer" ->
			inv[T5.PrinterPool] { self =>
				self.isColor implies self.getPrinter.exists (_.isColor) },
		// satisfied by t5-01, violated by t5-00

		"If a Printer pool contains a color scanner then it contains a color printer" ->
			inv[T6.PrinterPool] { self =>
				self.getScanner.exists (_.isColor) implies
					self.getPrinter.exists (_.isColor) },
		// satisfied by t6-01, t6-02 violated by t6-00

		"If a printer pool has a color scanner, all its printers are color printers" ->
			inv[T6.PrinterPool] { self =>
				self.getScanner.exists (_.isColor) implies
					self.getPrinter.forall (_.isColor) },
		// satisfied by t6-02 violated t6-00, t6-01

		"There is at most one color printer in any pool" ->
			inv[T6.PrinterPool] { _.getPrinter.filter {_.isColor}.size <= 1 }
		// satisfied by t6-00, t6-01 violated t6-02
	)

}
